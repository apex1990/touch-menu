import React from 'react';
import {AppRegistry, YellowBox} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import GlobalContext from './src/GlobalContext';

YellowBox.ignoreWarnings(['Warning: ...']);
console.disableYellowBox = true;

const Main = () => {
  return (
    <GlobalContext>
      <App />
    </GlobalContext>
  );
};

AppRegistry.registerComponent(appName, () => Main);
