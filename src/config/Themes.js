import {DefaultTheme} from 'react-native-paper';

const LightTheme = {
  ...DefaultTheme,
  dark: false,
  roundness: 0,
  elevation: 0,
  colors: {
    ...DefaultTheme.colors,
    background: '#fff',
    primary: 'red',
  },
};

const DarkTheme = {
  ...DefaultTheme,
  dark: true,
  roundness: 0,
  colors: {
    ...DefaultTheme.colors,
    background: '#000',
  },
};

export default isDark => {
  return isDark ? DarkTheme : LightTheme;
};
