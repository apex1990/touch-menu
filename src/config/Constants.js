// Keys
export const KEY_NEW_ORDERS = 'new_orders';
export const KEY_PENDING_ORDERS = 'pending_orders';
export const KEY_DONE_ORDERS = 'done_orders';

export const KEY_IMAGE_LOGO = 'image_logo';
export const KEY_ICON_DRAWER = 'icon_drawer';
export const KEY_ICON_NEW_ORDERS = 'icon_new_orders';
export const KEY_ICON_PENDING_ORDERS = 'icon_pending_orders';
export const KEY_ICON_DONE_ORDERS = 'icon_done_orders';
export const KEY_ICON_FOOD = 'icon_food';
export const KEY_ICON_DRINK = 'icon_drink';
export const KEY_NEW_ORDERS_BUTTON = 'icon_new_orders_button';
export const KEY_ALERTS_BUTTON = 'icon_alerts_button';
export const KEY_ALERT = 'alert';
export const KEY_ICON_CHECK = 'icon_check';

export const TYPE_SCREEN_NEW_ORDERS = 'type_screen_new_orders';
export const TYPE_SCREEN_PENDING_ORDERS = 'type_screen_pending_orders';
export const TYPE_SCREEN_READY_ORDERS = 'type_screen_ready_orders';

export const SETTINGS_LANGUAGE = 'settings_language';

export const NOTIFICATION_TYPE_REFRESH_NEW_ORDERS = 'type_refresh_new_orders';
export const NOTIFICATION_TYPE_SYNC_ORDERS = 'type_sync_orders';
export const NOTIFICATION_TYPE_SIGNAL = 'type_signal';
export const NOTIFICATION_TYPE_SIGNAL_CLEAR = 'type_signal_clear';
export const NOTIFICATION_TYPE_FORCE_LOGOUT = 'type_logout';

export const ALERT_INTERVAL = 30 * 1000;
