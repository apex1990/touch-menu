export const KEY_CONFIG = 'key_config';
export const KEY_LANG = 'key_lang';
export const KEY_URL = 'key_url';
export const KEY_USER = 'key_user';
export const KEY_LANGUAGES = "key_languages";
export const KEY_USER_LOCALE = "key_user_locale";
export const KEY_LOCALIZED_LABELS = "key_localized_labels";
