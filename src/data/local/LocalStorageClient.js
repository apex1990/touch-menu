import AsyncStorage from '@react-native-community/async-storage';

class LocalStorageClient {
  getData = async key => {
    try {
      return {data: JSON.parse(await AsyncStorage.getItem(key)), error: null};
    } catch (e) {
      return {data: null, error: e};
    }
  };

  storeData = async (key, data) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(data));
      return {success: true, error: null};
    } catch (e) {
      return {success: false, error: e};
    }
  };

  removeData = async key => {
    try {
      await AsyncStorage.removeItem(key);
      return {success: true, error: null};
    } catch (e) {
      return {success: false, error: e};
    }
  };
}

export default LocalStorageClient;
