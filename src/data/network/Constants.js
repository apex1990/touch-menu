export const API_URL_DEV = 'https://dev.touchmenuapp.com/api/v2/kbmod';
export const API_URL_PROD = 'https://touchmenuapp.com/api/v2/kbmod';

export const TYPE_ORDER_NEW = 'new';
export const TYPE_ORDER_PENDING = 'processing';
export const TYPE_ORDER_READY = 'ready';
export const TYPE_ORDER_DELIVERED = 'delivered';
export const TYPE_ORDER_PAID = 'paid';

export const getApiUrl = () => {
  //return __DEV__ ? API_URL_DEV : API_URL_PROD;
  return API_URL_PROD;
};
