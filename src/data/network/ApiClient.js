import axios from 'axios';

import md5 from 'md5';

class ApiClient {
  constructor(url, authMiddleware) {
    this.url = url;
    this.client = axios.create({
      baseURL: this.url,
      timeout: 2000,
    });

    this.client.interceptors.response.use(response => {
      var {error, force_logout} = response.data;

      if (error && force_logout) {
        authMiddleware();
        throw new axios.Cancel('Not authorized user.');
      }

      return response;
    });
  }

  getLanguages = async () => {
    const response = await this.client.get('languages');
    return this.formatResponseObj(response);
  };

  getLocalizedLabels = async () => {
    const response = await this.client.get("translations");
    return this.formatResponseObj(response);
  };

  updateConfigHeaders = (url, token, lang) => {
    if (this.client.defaults.baseURL != url) {
      this.client.defaults.baseURL = `${url}/`;
    }

    delete this.client.defaults.headers;
    if (token) {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    }

    if (lang) {
      axios.defaults.headers.common['Accept-Language'] = lang;
    }
  };

  login = async (username, password) => {
    var body = new FormData();
    body.append('username', username);
    body.append('password', md5(password));

    const response = await this.client.post('login', body);

    if (!response.data.error) {
      this.authToken = response.data.data.user.token;
    }

    return this.formatResponseObj(response);
  };

  logout = async () => {
    const response = await this.client.get('logout');
    return this.formatResponseObj(response);
  };

  getOrders = async type => {
    const response = await this.client.get(`orders/${type}`, {
      withCredentials: true,
    });
    return this.formatResponseObj(response);
  };

  getOrderDetails = async orderId => {
    const response = await this.client.get(`order/${orderId}`, {
      withCredentials: true,
    });
    return this.formatResponseObj(response);
  };

  changeOrderStatus = async orderId => {
    const response = await this.client.post(`order/${orderId}`, {
      withCredentials: true,
    });
    return this.formatResponseObj(response);
  };

  pendingOrderStatus = async orderId => {
    const response = await this.client.post(`add_order_to_pos/${orderId}`, {
      withCredentials: true,
    })
    return this.formatResponseObj(response);
  }

  updateFcmToken = async token => {
    var body = new FormData();
    body.append('token', token);

    const response = await this.client.post(`fcm`, body, {
      withCredentials: true,
    });
    return this.formatResponseObj(response);
  };

  changeVipClientStatus = async (id, newStatus) => {
    var body = new URLSearchParams();
    body.append('status', newStatus);

    const response = await this.client.put(`/vip/${id}`, body, {
      withCredentials: true,
    });
    return this.formatResponseObj(response);
  };

  getAlerts = async () => {
    const response = await this.client.get('alert/all', {
      withCredentials: true,
    });
    return this.formatResponseObj(response);
  };

  clearAlert = async id => {
    var body = new FormData();
    body.append('ids', id);

    const response = await this.client.post('alert/clear', body, {
      withCredentials: true,
    });

    return this.formatResponseObj(response);
  };

  formatResponseObj = response => {
    if (response.data.error) {
      return {
        error: true,
        message: response.data.text,
      };
    } else {
      return {
        error: false,
        data: response.data.data,
      };
    }
  };
}

export default ApiClient;
