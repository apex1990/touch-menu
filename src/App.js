import React, {useState, useEffect, useContext} from 'react';
import Router from './Router';
import KeepAwake from 'react-native-keep-awake';
import firebase from 'react-native-firebase';
import {
  NOTIFICATION_TYPE_SIGNAL,
  ALERT_INTERVAL,
  KEY_ALERT,
  NOTIFICATION_TYPE_REFRESH_NEW_ORDERS,
  NOTIFICATION_TYPE_SIGNAL_CLEAR,
} from './config/Constants';
import {GlobalContext} from './GlobalContext';
import Dialog from 'react-native-dialog';
import BackgroundTimer from 'react-native-background-timer';
import {TouchableWithoutFeedback, View, Image} from 'react-native';
import ImageUtils from './utils/ImageUtils';
import Sound from 'react-native-sound';

const App = () => {
  const {loginState, userRepository, alerts, counters} = useContext(
    GlobalContext,
  );
  const [isAlertDialogVisible, setAlertDialogVisible] = useState(false);
  const [notification, setNotification] = useState(null);
  const [isWarningVisible, setIsWarningVisible] = useState(false);
  const [alertSounds, setAlertSounds] = useState({
    alert: null,
    warning: null,
  });

  useEffect(() => {
    const alertSound = new Sound(
      'bell_notification.mp3',
      Sound.MAIN_BUNDLE,
      error => {
        if (!error) {
          const warningSound = new Sound(
            'bell_notification.mp3',
            Sound.MAIN_BUNDLE,
            error => {
              if (!error) {
                warningSound.setNumberOfLoops(30);
                setAlertSounds({
                  alert: alertSound,
                  warning: warningSound,
                });
              }
            },
          );
        }
      },
    );
  }, []);

  useEffect(() => {
    if (counters.alerts == 0 && counters.newOrders == 0) {
      BackgroundTimer.stopBackgroundTimer();
      setIsWarningVisible(false);
    }
  }, [counters.alerts, counters.newOrders]);

  useEffect(() => {
    setNotification(null);

    if (!isWarningVisible && alertSounds.warning) {
      alertSounds.warning.stop();
    } else if (isWarningVisible && alertSounds.warning && alertSounds.alert) {
      alertSounds.alert.stop(_ => alertSounds.warning.play());
    }
  }, [isWarningVisible]);

  useEffect(() => {
    if (!isAlertDialogVisible && alertSounds.alert) {
      alertSounds.alert.stop();
    } else if (isAlertDialogVisible && alertSounds.alert) {
      alertSounds.alert.play();
    }
  }, [isAlertDialogVisible]);

  useEffect(() => {
    KeepAwake.activate();

    if (loginState.isLoggedIn) {
      firebase.notifications().onNotification(notification => {
        const {type} = notification.data;
        if (
          type == NOTIFICATION_TYPE_SIGNAL ||
          type == NOTIFICATION_TYPE_REFRESH_NEW_ORDERS
        ) {
          alerts.onFetchRequest();
          setNotification(notification.data);
        }

        if (type == NOTIFICATION_TYPE_SIGNAL_CLEAR) {
          setNotification(null);
        }
      });
    } else {
      BackgroundTimer.stopBackgroundTimer();
    }
  }, [loginState.isLoggedIn]);

  useEffect(() => {
    if (!isWarningVisible && notification) {
      setAlertDialogVisible(true);
    } else {
      setAlertDialogVisible(false);
      setNotification(false);
    }
  }, [notification]);

  useEffect(() => {
    scheduleAlertIfNeeded();
  }, [counters.newOrders, counters.alerts]);

  const clearAlert = async id => {
    if (alertSounds.alert != null) {
      alertSounds.alert.stop();
    }

    const response = await userRepository.clearAlert(id);
    if (response.success) {
      alerts.onFetchRequest();
    }

    setAlertDialogVisible(false);
    setNotification(null);
  };

  const scheduleAlertIfNeeded = () => {
    BackgroundTimer.stopBackgroundTimer();

    let hasAlerts = counters.alerts > 0;
    let hasOrders = counters.newOrders > 0;

    if (hasOrders || hasAlerts) {
      BackgroundTimer.runBackgroundTimer(() => {
        BackgroundTimer.stopBackgroundTimer();
        setIsWarningVisible(true);
      }, ALERT_INTERVAL);
    }
  };

  return (
    <>
      <Router />
      <Dialog.Container visible={isAlertDialogVisible}>
        <Dialog.Title>
          {notification != null ? notification.title : ''}
        </Dialog.Title>
        <Dialog.Description>
          {notification != null ? notification.body : ''}
        </Dialog.Description>
        <Dialog.Button
          label="OK"
          onPress={() => {
            if (notification && notification.type == NOTIFICATION_TYPE_SIGNAL) {
              clearAlert(notification.signal_id);
            } else {
              if (alertSounds.alert != null) {
                alertSounds.alert.stop();
              }
              setAlertDialogVisible(false);
              setNotification(null);
            }
          }}
        />
      </Dialog.Container>
      {isWarningVisible ? (
        <TouchableWithoutFeedback
          onPress={() => {
            scheduleAlertIfNeeded();
            setIsWarningVisible(false);
          }}>
          <View
            style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              backgroundColor: 'black',
              opacity: 0.8,
            }}>
            <Image
              source={ImageUtils.loadImageWithKey(KEY_ALERT)}
              style={{resizeMode: 'center', width: '100%', height: '100%'}}
            />
          </View>
        </TouchableWithoutFeedback>
      ) : null}
    </>
  );
};

export default App;
