/* eslint-disable no-dupe-keys */
import React, {createContext, useState, useEffect} from 'react';
import ApiClient from './data/network/ApiClient';
import LocalStorageClient from './data/local/LocalStorageClient';
import UserRepository from './repositories/UsersRepository';
import LanguageRepository from './repositories/LanguageRepository';
import OrdersRepository from './repositories/OrdersRepository';
import {getApiUrl} from './data/network/Constants';
import {KEY_CONFIG, KEY_USER} from './data/local/Constants';
import {Provider as PaperProvider} from 'react-native-paper';
import GetAppTheme from './config/Themes';
import Locale, {Bg, En} from './localization';
import firebase from 'react-native-firebase';
import {
  NOTIFICATION_TYPE_SIGNAL_CLEAR,
  NOTIFICATION_TYPE_FORCE_LOGOUT,
} from './config/Constants';

export const GlobalContext = createContext(null);

const localStorageClient = new LocalStorageClient();

export default ({children}) => {
  const [theme, switchTheme] = useState(GetAppTheme(false));
  const [langCode, setLangCode] = useState();
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isConfigObtained, setConfigObtained] = useState(false);
  const [alerts, setAlerts] = useState([]);
  const [newOrders, setNewOrders] = useState(0);
  const [processedAlertId, setProcessedAlertId] = useState(-1);
  const [appLanguages, setAppLanguages] = useState([]);
  const [appLocale, setAppLocale] = useState();
  const [appTranslations, setAppTranslations] = useState();

  const apiClient = new ApiClient(getApiUrl(), () => {
    logout();
  });

  useEffect(() => {
    const reqLang = async () => {
      const langResponse = await apiClient.getLanguages();
      if (!langResponse.error) {
        const {languages} = langResponse.data;
        setAppLanguages(languages);
      }
    };
    reqLang();

    const reqTrans = async () => {
      const transResponse = await apiClient.getLocalizedLabels();
      if (!transResponse.error) {
        const {data} = transResponse;
        setAppTranslations(data);
      }
    };
    reqTrans();
  }, []);

  useEffect(() => {
    const reqLocale = async () => {
      const myLocale = await languageRepository.getUserLocale();
      if (myLocale.data) {
        setAppLocale(myLocale.data);
      } else {
        let userLocale = 'en_GB';
        if (appLanguages.length > 0) {
          appLanguages.map(language => {
            if (language.basic === 'yes') {
              userLocale = language.locale;
            }
          });
          await languageRepository.setUserLocale(userLocale);
          setAppLocale(userLocale);
        }
      }
    };
    reqLocale();
  }, [appLanguages]);

  useEffect(() => {
    if (processedAlertId != -1 && alerts.length > 0) {
      setAlerts(
        alerts.map(alert => {
          if (alert.id == processedAlertId) {
            alert.status = 'processed';
          }
          return alert;
        }),
      );
      setProcessedAlertId(-1);
    }
  }, [processedAlertId, alerts]);

  const logout = () => {
    setConfigObtained(false);
    setLoggedIn(false);
    Promise.all(
      localStorageClient.removeData(KEY_CONFIG),
      localStorageClient.removeData(KEY_USER),
    );
  };

  const userRepository = new UserRepository(apiClient, localStorageClient);
  const ordersRepository = new OrdersRepository(apiClient, localStorageClient);
  const languageRepository = new LanguageRepository(apiClient, localStorageClient);

  const locale = new Locale(langCode, ['bg_BG', 'en_GB'], [Bg, En], code =>
    setLangCode(code),
  );

  useEffect(() => {
    if (isLoggedIn) {
      const configRequest = async () => {
        const config = await localStorageClient.getData(KEY_CONFIG);
        if (config && config.data) {
          setLangCode(config.data.lang);

          apiClient.updateConfigHeaders(
            config.data.url,
            config.data.token,
            config.data.lang,
          );

          setConfigObtained(true);
        }
      };
      configRequest();

      const fcmConfigRequest = async () => {
        if (await firebase.messaging().hasPermission()) {
          const fcmToken = await firebase.messaging().getToken();
          await userRepository.updateFcmToken(fcmToken);

          firebase.messaging().onTokenRefresh(async fcmToken => {
            await userRepository.updateFcmToken(fcmToken);
          });
        }
      };
      fcmConfigRequest();

      firebase.messaging().onMessage(message => {
        if (message.data.type == NOTIFICATION_TYPE_SIGNAL_CLEAR) {
          setProcessedAlertId(message.data.signal_id);
        } else if (message.data.type == NOTIFICATION_TYPE_FORCE_LOGOUT) {
          logout();
        }
      });
    } else {
      setConfigObtained(false);
    }
  }, [isLoggedIn]);

  useEffect(() => {
    const configRequest = async () => {
      const config = await localStorageClient.getData(KEY_CONFIG);
      if (config && config.data) {
        const {data} = config;
        if (langCode && data.lang != langCode) {
          data.lang = langCode;
          const result = await localStorageClient.storeData(KEY_CONFIG, data);
          if (result.success) {
            apiClient.updateConfigHeaders(
              config.data.url,
              config.data.token,
              config.data.lang,
            );
          } else {
            console.log('Store Error:', result.error);
          }
        }
      }
    };
    configRequest();
  }, [langCode]);

  const onFetchAlertsRequest = () => {
    const request = async () => {
      const response = await userRepository.getAlerts();
      if (response.success && response.data && response.data.a) {
        setAlerts(response.data.a);
      }
    };
    request();
  };

  const GlobalStore = {
    ui: {theme: theme, switchTheme: switchTheme},
    userRepository: userRepository,
    ordersRepository: ordersRepository,
    languageRepository: languageRepository,
    locale: locale,
    loginState: {
      isLoggedIn: isLoggedIn,
      isConfigObtained: isConfigObtained,
      setLoggedIn: setLoggedIn,
    },
    alerts: {
      list: alerts,
      onFetchRequest: onFetchAlertsRequest,
    },
    counters: {
      alerts: alerts.filter(i => i.status == 'new').length,
      newOrders: newOrders,
      setNewOrders: setNewOrders,
    },
    appLanguages: appLanguages,
    appLocaleState: {
      locale: appLocale,
      setAppLocale: setAppLocale,
    },
    appTranslations: appTranslations,
  };

  return (
    <GlobalContext.Provider value={GlobalStore}>
      <PaperProvider theme={theme}>{children}</PaperProvider>
    </GlobalContext.Provider>
  );
};
