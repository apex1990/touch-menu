import {
  TYPE_ORDER_NEW,
  TYPE_ORDER_PENDING,
  TYPE_ORDER_READY,
} from '../data/network/Constants';

class OrdersRepository {
  constructor(apiClient, localStorageClient) {
    this.apiClient = apiClient;
    this.localStorageClient = localStorageClient;
  }

  getNewOrdersList = async () => {
    try {
      const response = await this.apiClient.getOrders(TYPE_ORDER_NEW);
      if (response && !response.error) {
        const orders = await this.fetchOrdersWithDetails(response.data.orders);
        return {
          success: true,
          data: {
            orders: orders,
          },
        };
      } else {
        return {
          success: false,
          data: {
            message: 'Cannot obtain the new orders list...',
          },
        };
      }
    } catch (e) {
      return {
        success: false,
        data: {
          message: 'Cannot obtain the new orders list...',
        },
      };
    }
  };

  getPendingOrdersList = async () => {
    try {
      const response = await this.apiClient.getOrders(TYPE_ORDER_PENDING);
      if (response && !response.error) {
        const orders = await this.fetchOrdersWithDetails(response.data.orders);
        return {
          success: true,
          data: {
            orders: orders,
          },
        };
      } else {
        return {
          success: false,
          data: {
            message: 'Cannot obtain the pending orders list...',
          },
        };
      }
    } catch (e) {
      return {
        success: false,
        data: {
          message: 'Cannot obtain the pending orders list...',
        },
      };
    }
  };

  getReadyOrdersList = async () => {
    try {
      const response = await this.apiClient.getOrders(TYPE_ORDER_READY);
      if (response && !response.error) {
        const orders = await this.fetchOrdersWithDetails(response.data.orders);
        return {
          success: true,
          data: {
            orders: orders,
          },
        };
      } else {
        return {
          success: false,
          data: {
            message: 'Cannot obtain the ready orders list...',
          },
        };
      }
    } catch (e) {
      return {
        success: false,
        data: {
          message: 'Cannot obtain the ready orders list...',
        },
      };
      x;
    }
  };

  changeOrderStatus = async (orderId, type) => {
    try {
      const response = await this.apiClient.changeOrderStatus(orderId);

      if (response && !response.error) {
        return {
          success: true,
          data: {
            expandedId: orderId,
          },
        };
      } else {
        return {
          success: false,
          data: {
            message: 'Cannot change the status of this order...',
          },
        };
      }
    } catch (e) {
      return {
        success: false,
        data: {
          message: 'Cannot change the status of this order...',
        },
      };
    }
  };

  pendingOrderStatus = async (orderId, type) => {
    try {
      const response = await this.apiClient.pendingOrderStatus(orderId);

      if (response && !response.error) {
        return {
          success: true,
          data: {
            expandedId: orderId,
          },
        };
      } else {
        return {
          success: false,
          data: {
            message: 'Cannot change the status of this order...',
          },
        };
      }
    } catch (e) {
      return {
        success: false,
        data: {
          message: 'Cannot change the status of this order...',
        },
      };
    }
  };

  fetchOrdersWithDetails = async orders => {
    let promise = orders.map(async order => {
      const orderDetailsResponse = await this.apiClient.getOrderDetails(
        order.d,
      );
      order.id = guidGenerator();
      if (orderDetailsResponse && !orderDetailsResponse.error) {
        order.items = Object.values(orderDetailsResponse.data.items);
        order.isCompleted = orderDetailsResponse.data.order_complete;
        order.isExpanded = false;
      } else {
        order.items = [];
        order.isCompleted = false;
        order.isExpanded = false;
      }
      return order;
    });
    return Promise.all(promise);
  };
}

guidGenerator = () => {
  var S4 = function() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  };
  return (
    S4() +
    S4() +
    '-' +
    S4() +
    '-' +
    S4() +
    '-' +
    S4() +
    '-' +
    S4() +
    S4() +
    S4()
  );
};

export default OrdersRepository;
