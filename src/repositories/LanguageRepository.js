import {KEY_USER_LOCALE} from '../data/local/Constants';

class LanguageRepository {
  constructor(apiClient, localStorageClient) {
    this.apiClient = apiClient;
    this.localStorageClient = localStorageClient;
  }

  getUserLocale = async () => {
    return await this.localStorageClient.getData(KEY_USER_LOCALE);
  };

  setUserLocale = async lang => {
    const result = await this.localStorageClient.storeData(
      KEY_USER_LOCALE,
      lang,
    );
    if (result.success) {
      return {
        success: true,
        data: lang,
      };
    } else {
      return {
        success: false,
        data: {
          message: 'Cannot set locale',
        },
      };
    }
  };
}

export default LanguageRepository;
