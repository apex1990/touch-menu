import {KEY_USER, KEY_CONFIG} from '../data/local/Constants';

class UserRepository {
  constructor(apiClient, localStorageClient) {
    this.apiClient = apiClient;
    this.localStorageClient = localStorageClient;
  }

  isLoggedIn = async () => {
    const userResult = await this.localStorageClient.getData(KEY_USER);
    const configResult = await this.localStorageClient.getData(KEY_CONFIG);

    if (userResult.data && configResult.data) {
      return {
        success: true,
        data: {
          user: userResult.data,
          config: configResult.data,
        },
      };
    } else {
      return {
        success: false,
        data: {
          message: 'The user is not logged in.',
        },
      };
    }
  };

  getUser = async () => {
    return await this.localStorageClient.getData(KEY_USER);
  };

  auth = async (username, password) => {
    const response = await this.apiClient.login(username, password);

    if (!response.error) {
      const {user, url, lang} = response.data;

      const userResult = await this.localStorageClient.storeData(
        KEY_USER,
        user,
      );
      const configResult = await this.localStorageClient.storeData(KEY_CONFIG, {
        url: `${url}/api/v2/kbmod`,
        lang: lang,
        token: user.token,
      });

      if (userResult.success && configResult.success) {
        return {
          success: true,
          data: {},
        };
      } else {
        return {
          success: false,
          data: {
            message: 'Cannot login now... Please try again later!',
          },
        };
      }
    } else {
      return {
        success: false,
        data: {
          message: 'Cannot login now... Please try again later!',
        },
      };
    }
  };

  logout = async () => {
    const response = await this.apiClient.logout();

    if (!response.error) {
      const userResult = await this.localStorageClient.removeData(KEY_USER);
      const configResult = await this.localStorageClient.removeData(KEY_CONFIG);
      if (userResult.success && configResult.success) {
        return {
          success: true,
          data: {},
        };
      } else {
        return {
          success: false,
          data: {
            message: 'Cannot logout the user...',
          },
        };
      }
    } else {
      return {
        success: false,
        data: {
          message: 'Cannot logout the user...',
        },
      };
    }
  };

  updateFcmToken = async token => {
    const response = await this.apiClient.updateFcmToken(token);

    if (!response.error) {
      console.log('Success', response);
    } else {
      console.log('Failure', response);
    }
  };

  changeVipClientStatus = async (id, newStatus) => {
    const response = await this.apiClient.changeVipClientStatus(id, newStatus);

    if (!response.error) {
      return {
        success: true,
        data: {},
      };
    } else {
      return {
        success: false,
        data: {
          message: response.message,
        },
      };
    }
  };

  getAlerts = async () => {
    const response = await this.apiClient.getAlerts();

    if (!response.error) {
      return {
        success: true,
        data: response.data,
      };
    } else {
      return {
        success: false,
        data: {
          message: response.message,
        },
      };
    }
  };

  clearAlert = async id => {
    const response = await this.apiClient.clearAlert(id);

    if (!response.error) {
      return {
        success: true,
        data: response.data,
      };
    } else {
      return {
        success: false,
        data: {
          message: response.message,
        },
      };
    }
  };
}

export default UserRepository;
