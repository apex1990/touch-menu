export default {
  code: 'en_GB',
  names: [
    {
      code: 'en_GB',
      name: 'English',
    },
    {
      code: 'bg_BG',
      name: 'Английски',
    },
  ],
  strings: [
    {
      key: 'username',
      str: 'Username',
    },
    {
      key: 'password',
      str: 'Password',
    },
    {
      key: 'login',
      str: 'Login',
    },
    {
      key: 'settings',
      str: 'Settings',
    },
    {
      key: 'logout',
      str: 'Logout',
    },
    {
      key: 'language',
      str: 'Language',
    },
    {
      key: 'dialog_language',
      str: 'Select a language:',
    },
    {
      key: 'tab_new',
      str: 'NEW',
    },
    {
      key: 'tab_pending',
      str: 'PENDING',
    },
    {
      key: 'tab_ready',
      str: 'READY',
    },
    {
      key: 'msg_err_enter_username',
      str: 'Please enter your username!',
    },
    {
      key: 'msg_err_enter_password',
      str: 'Please enter your password!',
    },
    {
      key: 'delivery_option',
      str: 'Delivery option',
    },
    {
      key: 'delivery_option_pickup',
      str: 'Pickup',
    },
    {
      key: 'delivery_option_delivery',
      str: 'Delivery',
    },
    {
      key: 'vip_status_confirmed',
      str: 'Confirmed',
    },
    {
      key: 'vip_status_unconfirmed',
      str: 'Unconfirmed',
    },
    {
      key: 'vip_client',
      str: 'VIP client',
    },
    {
      key: 'comment',
      str: 'COMMENT',
    },
    {
      key: 'details',
      str: 'DETAILS',
    },
    {
      key: 'approve',
      str: 'APPROVE',
    },
    {
      key: 'end',
      str: 'END',
    },
    {
      key: 'total',
      str: 'TOTAL',
    },
    {
      key: 'choosen_option',
      str: 'Choosen option: ',
    },
    {
      key: 'alerts',
      str: 'Alerts',
    }
  ],
};
