export default {
  code: 'bg_BG',
  names: [
    {
      code: 'bg_BG',
      name: 'Български',
    },
    {
      code: 'en_GB',
      name: 'Bulgarian',
    },
  ],
  strings: [
    {
      key: 'username',
      str: 'Потребителско име',
    },
    {
      key: 'password',
      str: 'Парола',
    },
    {
      key: 'login',
      str: 'Вход',
    },
    {
      key: 'settings',
      str: 'Настройки',
    },
    {
      key: 'logout',
      str: 'Изход',
    },
    {
      key: 'language',
      str: 'Език',
    },
    {
      key: 'dialog_language',
      str: 'Изберете език:',
    },
    {
      key: 'tab_new',
      str: 'НОВИ',
    },
    {
      key: 'tab_pending',
      str: 'ОБРАБОТКА',
    },
    {
      key: 'tab_ready',
      str: 'ГОТОВИ',
    },
    {
      key: 'msg_err_enter_username',
      str: 'Моля въведете потребителско име!',
    },
    {
      key: 'msg_err_enter_password',
      str: 'Моля въведете парола!',
    },
    {
      key: 'delivery_option',
      str: 'Начин на поръчка',
    },
    {
      key: 'delivery_option_pickup',
      str: 'Взимане от място',
    },
    {
      key: 'delivery_option_delivery',
      str: 'Доставка',
    },
    {
      key: 'vip_status_confirmed',
      str: 'Потвърден',
    },
    {
      key: 'vip_status_unconfirmed',
      str: 'Непотвърден',
    },
    {
      key: 'vip_client',
      str: 'VIP клиент',
    },
    {
      key: 'comment',
      str: 'КОМЕНТАР',
    },
    {
      key: 'details',
      str: 'ДЕТАЙЛИ',
    },
    {
      key: 'approve',
      str: 'Oдобри',
    },
    {
      key: 'end',
      str: 'ПРИКЛЮЧИ',
    },
    {
      key: 'total',
      str: 'ОБЩО',
    },
    {
      key: 'choosen_option',
      str: 'Избрана опция: ',
    },
    {
      key: 'alerts',
      str: 'Alerts',
    }
  ],
};
