class Locale {
  constructor(
    defaultLangCode,
    supportedLanguages,
    languageDefinitions,
    onLanguageChange,
  ) {
    this.langCode = defaultLangCode;
    this.supportedLanguages = supportedLanguages;
    this.languageDefinitions = languageDefinitions;
    this.onLanguageChange = onLanguageChange;
  }

  getCurrentLocale = () => {
    return this.langCode;
  };

  changeLanguage = langCode => {
    if (this.supportedLanguages.findIndex(l => l !== langCode) == -1)
      throw new Error('Unsupported language!');

    this.langCode = langCode;
    this.onLanguageChange(this.langCode);
  };

  getLocalizedString = (key, defaultStr) => {
    const def = this.languageDefinitions.find(d => d.code == this.langCode);

    if (def) {
      const rec = def.strings.find(r => r.key === key);
      return rec ? rec.str : defaultStr;
    } else {
      return defaultStr;
    }
  };

  getSupportedLanguages = () => {
    const languages = [];
    if (this.langCode == null) return languages;

    this.languageDefinitions.forEach(lang => {
      languages.push({
        code: lang.code,
        name: lang.names.find(l => l.code == this.langCode).name,
      });
    });
    return languages;
  };

  getTranslatedString = (key, defStr, locale, translations) => {
    if (translations) {
      const rec = translations.find(r => r.code === key);
      return rec ? rec[locale] : defStr;
    } else {
      return defStr;
    }
  };
}

export default Locale;
