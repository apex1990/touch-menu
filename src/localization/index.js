import Bg from './Bg';
import En from './En';
import Locale from './Locale';

export default Locale;
export {Bg, En};
