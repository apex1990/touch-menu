import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import AuthScreen from './screens/auth/AuthScreen';
import HomeScreen from './screens/home/HomeScreen';
import SettingsScreen from './screens/settings/SettingsScreen';
import AlertsScreen from './screens/alets/AlertsScreen';

const AppStackNavigator = createStackNavigator(
  {
    Auth: AuthScreen,
    Main: HomeScreen,
    Settings: SettingsScreen,
    Alerts: AlertsScreen,
  },
  {
    headerMode: 'none',
  },
);

export default createAppContainer(AppStackNavigator);
