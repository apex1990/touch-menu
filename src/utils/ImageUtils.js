import {
  KEY_ICON_NEW_ORDERS,
  KEY_ICON_PENDING_ORDERS,
  KEY_ICON_DONE_ORDERS,
  KEY_IMAGE_LOGO,
  KEY_ICON_DRAWER,
  KEY_ICON_FOOD,
  KEY_ICON_DRINK,
  KEY_NEW_ORDERS_BUTTON,
  KEY_ALERTS_BUTTON,
  KEY_ALERT,
  KEY_ICON_CHECK,
} from '../config/Constants';

class ImageUtils {
  static loadImageWithKey = key => {
    switch (key) {
      case KEY_IMAGE_LOGO: {
        return require('../../res/images/logo.png');
      }
      case KEY_ICON_DRAWER: {
        return require('../../res/images/ic_drawer.png');
      }
      case KEY_ICON_NEW_ORDERS: {
        return require('../../res/images/ic_new.png');
      }
      case KEY_ICON_PENDING_ORDERS: {
        return require('../../res/images/ic_processing.png');
      }
      case KEY_ICON_DONE_ORDERS: {
        return require('../../res/images/ic_ready.png');
      }
      case KEY_ICON_FOOD: {
        return require('../../res/images/ic_food.png');
      }
      case KEY_ICON_DRINK: {
        return require('../../res/images/ic_drink.png');
      }
      case KEY_NEW_ORDERS_BUTTON: {
        return require('../../res/images/ic_new_orders.png');
      }
      case KEY_ALERTS_BUTTON: {
        return require('../../res/images/ic_alert.png');
      }
      case KEY_ALERT: {
        return require('../../res/images/warning.gif');
      }
      case KEY_ICON_CHECK: {
        return require('../../res/images/ic_check.png');
      }

      default:
        throw new Error(`There is no such icon found for this key ${key}`);
    }
  };
}

export default ImageUtils;
