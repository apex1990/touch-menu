import {StyleSheet} from 'react-native';

export default theme => {
  return StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
      alignContent: 'stretch',
      padding: 16,
      backgroundColor: theme.colors.background,
    },
    logo: {
      width: '60%',
      height: 200,
      marginBottom: 32,
      alignSelf: 'center',
    },
    form: {
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
      alignContent: 'stretch',
      backgroundColor: '#fff',
    },
    textField: {
      marginTop: 4,
      marginStart: 16,
      marginEnd: 16,
      marginBottom: 4,
    },
    loginButton: {
      marginTop: 32,
      marginStart: 16,
      marginEnd: 16,
      elevation: 0,
    },
    loginButtonContent: {
      height: 48,
    },
  });
};
