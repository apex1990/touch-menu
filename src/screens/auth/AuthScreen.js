import React, {useContext, useState, useEffect, useMemo} from 'react';
import {
  SafeAreaView,
  Image,
  KeyboardAvoidingView,
  ToastAndroid,
} from 'react-native';
import {
  withNavigation,
  StackActions,
  NavigationActions,
} from 'react-navigation';
import useStyle from './AuthScreen.styles';
import {GlobalContext} from '../../GlobalContext';
import {TextInput, Button, Snackbar} from 'react-native-paper';
import Animated, {Easing} from 'react-native-reanimated';
import ImageUtils from '../../utils/ImageUtils';
import {KEY_IMAGE_LOGO} from '../../config/Constants';

const AuthScreen = ({navigation}) => {
  const {
    ui,
    userRepository,
    appLocaleState,
    appTranslations,
    locale,
    loginState,
  } = useContext(GlobalContext);

  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const [logoYPosition, setLogoYPosition] = useState(new Animated.Value(150));
  const [formOpacity, setFormOpacity] = useState(new Animated.Value(0));

  const styles = useStyle(ui.theme);

  useEffect(() => {
    const request = async () => {
      const result = await userRepository.isLoggedIn();
      loginState.setLoggedIn(result.success);
    };
    request();
  }, []);

  useEffect(() => {
    if (loginState.isLoggedIn && loginState.isConfigObtained) {
      navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Main'})],
        }),
      );
      cleanUp();
    } else if (!loginState.isLoggedIn) {
      logoTransitionAnimation().start(() => formOpacityAnimation().start());
    }
  }, [loginState]);

  const formOpacityAnimation = () => {
    return Animated.timing(formOpacity, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear,
    });
  };

  const logoTransitionAnimation = () => {
    return Animated.timing(logoYPosition, {
      toValue: 0,
      duration: 300,
      easing: Easing.linear,
    });
  };

  const onLoginAction = async () => {
    if (validateFields()) {
      const result = await userRepository.auth(username, password);
      if (result.success) {
        loginState.setLoggedIn(true);
      } else {
        ToastAndroid.show(result.data.message, ToastAndroid.SHORT);
      }
    }
  };

  const validateFields = () => {
    if (username == null) {
      ToastAndroid.show(
        locale.getTranslatedString(
          'msg_err_enter_username',
          'Please enter an username!',
          appLocaleState.locale,
          appTranslations,
        ),
        ToastAndroid.SHORT,
      );
      return false;
    }

    if (password == null) {
      ToastAndroid.show(
        locale.getTranslatedString(
          'msg_err_enter_password',
          'Please enter a password!',
          appLocaleState.locale,
          appTranslations,
        ),
        ToastAndroid.SHORT,
      );
      return false;
    }

    return true;
  };

  const cleanUp = () => {
    setUsername(null);
    setPassword(null);
  };

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView behavior="padding" enabled>
        <Animated.View style={{transform: [{translateY: logoYPosition}]}}>
          <Image
            source={ImageUtils.loadImageWithKey(KEY_IMAGE_LOGO)}
            style={styles.logo}
          />
        </Animated.View>
        <Animated.View style={{...styles.form, opacity: formOpacity}}>
          <TextInput
            label={locale.getTranslatedString(
              'username',
              'Username',
              appLocaleState.locale,
              appTranslations,
            )}
            mode="outlined"
            numberOfLines={1}
            autoCapitalize="none"
            value={username}
            onChangeText={text => setUsername(text)}
            style={styles.textField}
          />
          <TextInput
            label={locale.getTranslatedString(
              'password',
              'Password',
              appLocaleState.locale,
              appTranslations,
            )}
            mode="outlined"
            numberOfLines={1}
            value={password}
            secureTextEntry={true}
            onChangeText={text => setPassword(text)}
            style={styles.textField}
          />
          <Button
            mode="contained"
            onPress={onLoginAction}
            style={styles.loginButton}
            contentStyle={styles.loginButtonContent}>
            {locale.getTranslatedString(
              'login',
              'Login',
              appLocaleState.locale,
              appTranslations,
            )}
          </Button>
        </Animated.View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default withNavigation(AuthScreen);
