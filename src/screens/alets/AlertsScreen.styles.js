import {StyleSheet} from 'react-native';

export default theme =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: theme.colors.background,
    },
    appBar: {
      backgroundColor: 'transparent',
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: 'stretch',
      alignContent: 'stretch',
      elevation: 0,
    },
    appBarHeader: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: 'transparent',
    },
    appBarTitle: {
      flex: 1,
    },
    list: {
      flex: 1,
      padding: 8,
    },
    listItem: {
      marginTop: 4,
      marginBottom: 4,
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignContent: 'stretch',
      alignItems: 'flex-end',
      borderBottomColor: '#eee',
      borderBottomWidth: 1,
    },
    itemTitle: {
      alignSelf: 'flex-start',
      textAlignVertical: 'center',
      marginStart: 8,
      marginEnd: 8,
      fontWeight: 'bold',
      fontSize: 12,
    },
    itemBody: {
      alignSelf: 'flex-start',
      textAlignVertical: 'center',
      marginStart: 8,
      marginEnd: 8,
      marginTop: 4,
      marginBottom: 4,
      fontWeight: '600',
    },
    itemAction: {},
  });
