import React, {useContext, useState, useEffect} from 'react';
import {SafeAreaView, View, FlatList} from 'react-native';
import {withNavigation} from 'react-navigation';
import {Appbar, Text, Divider, Button} from 'react-native-paper';
import useStyles from './AlertsScreen.styles';
import {GlobalContext} from '../../GlobalContext';

const AlertsScreen = ({navigation}) => {
  const {ui, locale, userRepository, appLocaleState, appTranslations, alerts} = useContext(GlobalContext);
  const styles = useStyles(ui.theme);

  const [alertsList, setAlertsList] = useState([]);

  const handleClearAlert = async id => {
    const response = await userRepository.clearAlert(id);
    if (response.success) {
      alerts.onFetchRequest();
    }
  };

  const handleClearAllAlerts = async () => {
    var alertsToBeCleared = '';
    alertsList
      .filter(i => i.status == 'new')
      .map(i => i.id)
      .forEach(i => (alertsToBeCleared += `${i},`));

    alertsToBeCleared = alertsToBeCleared.substr(
      0,
      alertsToBeCleared.length - 1,
    );

    const response = await userRepository.clearAlert(alertsToBeCleared);
    if (response.success) {
      alerts.onFetchRequest();
    }
  };

  useEffect(() => {
    setAlertsList(alerts.list);
  }, [alerts.list]);

  const renderAlertsItem = (item, index) => {
    return (
      <View
        style={{
          ...styles.listItem,
          backgroundColor: item.status == 'new' ? '#eee' : '#fff',
        }}>
        <Text style={styles.itemTitle}>{item.c}</Text>
        <Text style={styles.itemBody}>{item.txt}</Text>
        {item.status == 'new' ? (
          <Button
            style={styles.itemAction}
            onPress={() => handleClearAlert(item.id)}>
            OK
          </Button>
        ) : null}
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <Appbar style={styles.appBar}>
        <Appbar.Header style={styles.appBarHeader}>
          <Appbar.BackAction onPress={() => navigation.goBack()} />
          <Text style={styles.appBarTitle}>
            {locale.getTranslatedString(
              'alerts',
              'Alerts',
              appLocaleState.locale,
              appTranslations,
            )}
          </Text>
          {alertsList.filter(i => i.status == 'new').length > 0 ? (
            <Appbar.Action
              icon="notification-clear-all"
              onPress={() => handleClearAllAlerts()}
            />
          ) : null}
        </Appbar.Header>
      </Appbar>

      <View style={{flex: 1}}>
        <FlatList
          data={alertsList}
          extraData={alertsList}
          renderItem={({item, index}) => renderAlertsItem(item, index)}
          keyExtractor={({_, index}) => String(index)}
          style={styles.list}
        />
      </View>
    </SafeAreaView>
  );
};

export default withNavigation(AlertsScreen);
