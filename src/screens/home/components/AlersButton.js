import React from 'react';
import {View, Image, TouchableWithoutFeedback} from 'react-native';
import styles from './NewOrdersButton.styles';
import ImageUtils from '../../../utils/ImageUtils';
import {KEY_ALERTS_BUTTON} from '../../../config/Constants';
import {Badge} from 'react-native-paper';

const AlertsButton = ({style, alertsCount, onPress}) => {
  const badgeView =
    alertsCount > 0 ? <Badge style={styles.badge}>{alertsCount}</Badge> : null;

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[styles.container, style]}>
        <Image
          source={ImageUtils.loadImageWithKey(KEY_ALERTS_BUTTON)}
          style={styles.icon}
        />
        {badgeView}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default AlertsButton;
