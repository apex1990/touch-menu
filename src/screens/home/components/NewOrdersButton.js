import React from 'react';
import {View, Image, TouchableWithoutFeedback} from 'react-native';
import styles from './NewOrdersButton.styles';
import ImageUtils from '../../../utils/ImageUtils';
import {KEY_NEW_ORDERS_BUTTON} from '../../../config/Constants';
import {Badge} from 'react-native-paper';

const NewOrdersButton = ({style, ordersCount, onPress}) => {
  const badgeView =
    ordersCount > 0 ? <Badge style={styles.badge}>{ordersCount}</Badge> : null;

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[styles.container, style]}>
        <Image
          source={ImageUtils.loadImageWithKey(KEY_NEW_ORDERS_BUTTON)}
          style={styles.icon}
        />
        {badgeView}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default NewOrdersButton;
