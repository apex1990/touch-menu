import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    width: 30,
    height: 30,
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
  },
  icon: {
    width: 20,
    height: 24,
  },
  badge: {
    position: 'absolute',
    top: 0,
    right: 0,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});
