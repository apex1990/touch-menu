import {StyleSheet} from 'react-native';

export default theme =>
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'stretch',
      alignContent: 'stretch',
    },
    containerSummary: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'stretch',
      alignContent: 'stretch',
      minHeight: 64,
    },
    table: {
      flex: 0.23,
      height: '100%',
      fontSize: 12,
      textAlignVertical: 'center',
      textAlign: 'center',
      alignSelf: 'center',
    },
    containerInfo: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'flex-start',
      alignContent: 'center',
      padding: 8,
    },
    containerPrice: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    priceLabel: {
      fontWeight: 'bold',
    },
    price: {
      marginStart: 8,
      backgroundColor: '#757575',
      paddingTop: 2,
      paddingBottom: 2,
      paddingStart: 4,
      paddingEnd: 4,
      borderRadius: 5,
      color: '#fff',
    },
    containerComment: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 4,
    },
    commentLabel: {
      fontWeight: 'bold',
      marginEnd: 8,
    },
    comment: {
      width: '60%',
    },
    counter: {
      width: 24,
      height: 24,
      margin: 8,
      paddingTop: 2,
      alignSelf: 'center',
      backgroundColor: '#FFF',
      borderRadius: 12,
      borderColor: '#757575',
      borderWidth: 1.5,
      color: '#757575',
      textAlign: 'center',
    },
    containerExpanded: {
      flexDirection: 'column',
      padding: 8,
    },
    containerDetails: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    detailsLabel: {
      fontWeight: 'bold',
      alignSelf: 'flex-start',
    },
    destinationContainer: {
      alignSelf: 'flex-start',
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: 4,
      paddingTop: 2,
      paddingBottom: 2,
      paddingStart: 4,
      paddingEnd: 4,
      backgroundColor: '#757575',
      borderRadius: 5,
    },
    destinationLabel: {
      marginStart: 16,
      color: '#fff',
    },
    containerOrderItem: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'stretch',
      alignContent: 'stretch',
      paddingStart: 8,
      marginBottom: 16,
    },
    choosenOptionLabel: {
      backgroundColor: '#3498dc',
      color: '#ffffff',
      paddingStart: 10,
      paddingEnd: 10,
      paddingTop: 2,
      paddingBottom: 2,
      alignSelf: 'flex-start',
      marginBottom: 8,
    },
    completeOrderButton: {
      alignSelf: 'flex-end',
      width: 160,
      marginEnd: 8,
      borderRadius: 20,
      backgroundColor: '#757575',
      color: '#fff',
    },
    pendingButton: {
      alignSelf: 'flex-start',
      width: 160,
      marginEnd: 8,
      borderRadius: 20,
      backgroundColor: '#57B471',
      color: '#fff',
    },
    confirmVipClientButton: {
      width: 100,
      marginTop: 8,
      borderRadius: 20,
      alignContent: 'center',
    },
    discount: {
      marginStart: 8,
      color: '#B81201',
    },
    timestamp: {
      flex: 0.2,
      textAlignVertical: 'center',
      textAlign: 'center',
    },
  });
