import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableWithoutFeedback,
  ToastAndroid,
} from 'react-native';
import {Text, Button} from 'react-native-paper';
import {
  TYPE_SCREEN_PENDING_ORDERS,
  KEY_ICON_FOOD,
  KEY_ICON_DRINK,
} from '../../../config/Constants';
import useStyle from './OrderItem.styles';
import {GlobalContext} from '../../../GlobalContext';
import ImageUtils from '../../../utils/ImageUtils';
import moment from 'moment';

class OrderItem extends Component {
  constructor(props) {
    super(props);

    const {
      theme,
      data,
      type,
      locale,
      appTranslations,
      appLocaleState,
      onExpansionStateChange,
      onCompleteOrderAction,
      onPendingOrderAction,
      refreshOrdersList,
      visibleFlag
    } = props

    this.state = {
      theme: theme,
      isExpanded: data.isExpanded,
      data: data,
      type: type,
      locale: locale,
      appTranslations: appTranslations,
      appLocaleState: appLocaleState,
      onExpansionStateChange: onExpansionStateChange,
      onCompleteOrderAction: onCompleteOrderAction,
      onPendingOrderAction: onPendingOrderAction,
      refreshOrdersList: refreshOrdersList,
      visibleFlag: true
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.isExpanded != nextState.isExpanded;
  }

  static getDerivedStateFromProps(props, state) {
    return Object.assign({}, state, {
      data: props.data,
      appTranslations: props.appTranslations,
      appLocaleState: props.appLocaleState,
      isExpanded: props.data.isExpanded,
    });
  }

  handleConfirmVipClientAction = async vipClient => {
    const {userRepository} = this.context;
    const result = await userRepository.changeVipClientStatus(
      vipClient.id,
      'confirmed',
    );
    if (result.success) {
      this.state.refreshOrdersList();
    } else {
      ToastAndroid.show(result.data.message, ToastAndroid.LONG);
    }
  };

  extractCommentView = (styles, data) => {
    const commentView = data.cm.length > 0 ? <Text>{data.cm}</Text> : null;
    return commentView;
  };

  extractDeliveryOptionView = (styles, data, shouldEllipsize) => {
    const {locale, appLocaleState, appTranslations} = this.state;
    const marginTop = shouldEllipsize ? 0 : 8;

    var deliveryOption = null;
    var deliveryName = null;
    var deliveryPhone = null;
    var deliveryAddress = null;

    if (data.delivery) {
      let deliveryText = (
        <Text style={{fontWeight: 'bold'}} ellipsizeMode="tail">
          {data.delivery.option == 'pickup'
            ? locale.getTranslatedString(
                'delivery_option_pickup',
                'Pickup',
                appLocaleState.locale,
                appTranslations,
              )
            : locale.getTranslatedString(
                'delivery_option_delivery',
                'Delivery',
                appLocaleState.locale,
                appTranslations,
              )}
        </Text>
      );

      if (data.delivery.name) {
        deliveryName = (
          <Text style={{fontWeight: 'bold', marginTop: 2}}>
            {data.delivery.name}
          </Text>
        );
      }

      if (data.delivery.phone) {
        deliveryPhone = (
          <Text style={{marginTop: 2}}>✆ {data.delivery.phone}</Text>
        );
      }

      if (data.delivery.address) {
        deliveryAddress = (
          <Text style={{marginTop: 2}}>☖ {data.delivery.address}</Text>
        );
      }

      deliveryOption = (
        <View>
          <Text
            numberOfLines={1}
            style={{
              marginTop: marginTop,
            }}>
            {locale.getTranslatedString(
              'delivery_option',
              'Delivery option',
              appLocaleState.locale,
              appTranslations,
            )}
            {deliveryText}
          </Text>
          {!shouldEllipsize && deliveryName}
          {!shouldEllipsize && deliveryAddress}
          {!shouldEllipsize && deliveryPhone}
        </View>
      );
    }
    return deliveryOption;
  };

  extractVipClientView = (styles, data) => {
    const {locale, appLocaleState, appTranslations} = this.state;

    var vipClientView = null;
    if (data.vip) {
      let statusText =
        data.vip.status == 'confirmed'
          ? locale.getTranslatedString(
              'vip_status_confirmed',
              'Confirmed',
              appLocaleState.locale,
              appTranslations,
            )
          : locale.getTranslatedString(
              'vip_status_unconfirmed',
              'Unconfirmed',
              appLocaleState.locale,
              appTranslations,
            );

      vipClientView = (
        <View style={{flexDirection: 'column'}}>
          <View
            style={{
              marginTop: 12,
              flexDirection: 'row',
            }}>
            <Text style={{fontWeight: 'bold'}}>
              {locale.getTranslatedString(
                'vip_client',
                'VIP client',
                appLocaleState.locale,
                appTranslations,
              )}
              :
            </Text>
            <Text style={{marginStart: 2}}>{statusText}</Text>
          </View>
          <Text style={{fontWeight: 'bold', marginTop: 2}}>
            {data.vip.name}
          </Text>
          {data.vip.phone.length > 0 ? (
            <Text style={{marginTop: 2}}>{'✆ '.concat(data.vip.phone)}</Text>
          ) : null}
          {data.vip.email.length > 0 ? (
            <Text style={{marginTop: 2}}>{'✉ '.concat(data.vip.email)}</Text>
          ) : null}
          {data.vip.status == 'unconfirmed' ? (
            <Button
              mode="outlined"
              compact="true"
              labelStyle={{
                fontSize: 11,
              }}
              style={styles.confirmVipClientButton}
              onPress={() => this.handleConfirmVipClientAction(data.vip)}>
              Потвърди
            </Button>
          ) : null}
        </View>
      );
    }
    return vipClientView;
  };

  showExpandedStateView = (styles, data) => {
    return (
      <View style={{flexDirection: 'column'}}>
        {this.extractCommentView(styles, data)}
        {this.extractDeliveryOptionView(styles, data, false)}
        {this.extractVipClientView(styles, data)}
      </View>
    );
  };

  showCollapsedStateView = (styles, data) => {
    return data.cm.length > 0 ? (
      <Text
        ellipsizeMode="tail"
        numberOfLines={1}
        style={{
          ...styles.comment,
          width: '60%',
        }}>
        {data.cm}
      </Text>
    ) : (
      this.extractDeliveryOptionView(styles, data, true)
    );
  };

  showCommentIfAvailable = styles => {
    const {locale, appLocaleState, appTranslations} = this.state;
    const {data, isExpanded} = this.state;

    return data.cm.length > 0 || data.delivery || data.vip ? (
      <View
        style={{
          ...styles.containerComment,
          flexDirection: isExpanded ? 'column' : 'row',
          alignItems: isExpanded ? 'flex-start' : 'center',
        }}>
        {data.cm.length > 0 ? (
          <Text style={styles.commentLabel}>
            {locale.getTranslatedString(
              'comment',
              'COMMENT',
              appLocaleState.locale,
              appTranslations,
            )}
            :
          </Text>
        ) : null}

        {isExpanded
          ? this.showExpandedStateView(styles, data)
          : this.showCollapsedStateView(styles, data)}
      </View>
    ) : null;
  };

  showDetailsContainerIfNeeded = (styles) => {
    const {
      isExpanded,
      type,
      data,
      locale,
      appLocaleState,
      appTranslations,
    } = this.state;
    console.log('visible container : ', this.state.visibleFlag)
    return isExpanded ? (
      <View style={styles.containerExpanded}>
        <Text style={styles.detailsLabel}>
          {locale.getTranslatedString(
            'details',
            'DETAILS',
            appLocaleState.locale,
            appTranslations,
          )}
          :
        </Text>
        {this.renderItems(styles)}
        {type == TYPE_SCREEN_PENDING_ORDERS ? (
          <View style={{display: 'flex', flexDirection: 'row'}}>
            {/* {
              this.state.visibleFlag === undefined || true ?
              ( */}
              <Button
              mode="contained"
              disabled={!this.state.visibleFlag}
              style={styles.pendingButton}
              onPress={() => this.handlePendingAction(data, type)}>
              {locale.getTranslatedString(
                'approve',
                'APPROVE',
                appLocaleState.locale,
                appTranslations,
              )}
            </Button>
            {/* ) : (<Button style={[styles.pendingButton, {backgroundColor: '#e6e6e6'}]} disabled={this}></Button>)
            } */}
            <Button
              mode="contained"
              style={styles.completeOrderButton}
              onPress={() => this.handleCompleteOrderAction(data, type)}>
              {locale.getTranslatedString(
                'end',
                'END',
                appLocaleState.locale,
                appTranslations,
              )}
            </Button>
          </View>
        ) : null}
      </View>
    ) : null;
  };

  renderItems = styles => {
    const {data, locale, appLocaleState, appTranslations} = this.state;

    return data.items.map(item => {
      const withItems = item.a
        ? item.a.map(i => {
            return <Text style={{color: '#2D7D21'}}>+ {i}</Text>;
          })
        : null;

      const withoutItems = item.r
        ? item.r.map(i => {
            return <Text style={{color: '#B81201'}}>- {i}</Text>;
          })
        : null;

      return (
        <View>
          <View style={styles.containerDetails}>
            <View style={styles.destinationContainer}>
              <Image
                source={ImageUtils.loadImageWithKey(
                  item.t == 'food' ? KEY_ICON_FOOD : KEY_ICON_DRINK,
                )}
                style={{width: 16, height: 16}}
              />
              <Text style={styles.destinationLabel}>{item.id}</Text>
            </View>
            <View style={styles.containerOrderItem}>
              <Text style={{flex: 0.8}}>
                {item.q} x {item.n}
              </Text>
              <Text style={{flex: 0.2, textAlign: 'right'}}>
                {item.v} {item.u}
              </Text>
            </View>
          </View>
          {item.o ? (
            <Text style={styles.choosenOptionLabel}>
              {locale.getTranslatedString(
                'choosen_option',
                'Choosen option:',
                appLocaleState.locale,
                appTranslations,
              )}
              {item.o}
            </Text>
          ) : null}
          <View style={{flexDirection: 'column'}}>{withItems}</View>
          <View>{withoutItems}</View>
        </View>
      );
    });
  };

  handlePressAction = () => {
    const {type, data, isExpanded} = this.state;
    this.state.onExpansionStateChange(type, data, !isExpanded);
  };

  handleCompleteOrderAction = (order, orderType) => {
    this.state.onCompleteOrderAction(orderType, order);
  };

  handlePendingAction = (order, orderType) => {
    this.state.onPendingOrderAction(orderType, order);
    this.setState({
      visibleFlag: false
    })
    console.log('click : ', this.state.visibleFlag)
    this.showDetailsContainerIfNeeded(useStyle(this.state.theme))
  };

  render() {
    const {
      theme,
      data,
      isExpanded,
      locale,
      appLocaleState,
      appTranslations,
    } = this.state;
    const styles = useStyle(theme);

    var typeStr = '';
    if (data.t) {
      typeStr = data.t;
    } else if (data.vip) {
      typeStr = locale.getTranslatedString(
        'vip_client',
        'VIP Client',
        appLocaleState.locale,
        appTranslations,
      );
    }

    var discount = null;
    if (data.dsc && data.dsc != '') {
      discount = (
        <Text style={styles.discount}>
          {data.dsc} {data.c}
        </Text>
      );
    }

    return (
      <TouchableWithoutFeedback
        key={data.id}
        onPress={() => this.handlePressAction()}>
        <View
          style={{
            ...styles.container,
            backgroundColor: isExpanded ? '#e6e6e6' : 'transparent',
          }}>
          <View style={styles.containerSummary}>
            <Text style={{...styles.table, backgroundColor: '#' + data.hex}}>
              {typeStr}
            </Text>
            <View style={styles.containerInfo}>
              <View style={styles.containerPrice}>
                <Text style={styles.priceLabel}>
                  {locale.getTranslatedString(
                    'total',
                    'TOTAL',
                    appLocaleState.locale,
                    appTranslations,
                  )}
                  :
                </Text>
                <Text style={styles.price}>{`${data.p} ${data.c}`}</Text>
                {discount}
                <Text style={styles.counter}>{data.i}</Text>
              </View>
              {this.showCommentIfAvailable(styles)}
            </View>
            <Text style={styles.timestamp}>
              {moment(data.u).format('DD/MM HH:mm')}
            </Text>
          </View>
          {this.showDetailsContainerIfNeeded(styles)}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

OrderItem.contextType = GlobalContext;

export default OrderItem;
