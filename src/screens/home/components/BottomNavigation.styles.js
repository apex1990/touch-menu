import {StyleSheet} from 'react-native';

export default theme =>
  StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignContent: 'stretch',
      height: 64,
      backgroundColor: theme.colors.primary,
    },
    tabContainer: {
      flex: 1,
    },
    tab: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignContent: 'stretch',
      alignItems: 'center',
      padding: 4,
    },
    tabLabel: {
      color: '#fff',
      marginTop: 4,
      flex: 0.5,
    },
    iconWrapper: {
      flex: 0.5,
      flexDirection: 'row',
      alignItems: 'flex-end',
      marginTop: 4,
    },
    icon: {
      width: 24,
      height: 24,
      tintColor: '#fff',
    },
  });
