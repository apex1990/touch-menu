import React, {useState, useEffect} from 'react';
import {View, Image} from 'react-native';
import useStyles from './BottomNavigation.styles';
import {Text, TouchableRipple} from 'react-native-paper';
import {
  KEY_ICON_NEW_ORDERS,
  KEY_ICON_PENDING_ORDERS,
  KEY_ICON_DONE_ORDERS,
} from '../../../config/Constants';
import ImageUtils from '../../../utils/ImageUtils';

const BottomNavigation = ({
  theme,
  locale,
  onTabPress,
  appLocaleState,
  appTranslations,
  viewPagerPos,
}) => {
  const styles = useStyles(theme);
  const [selectedTab, setSelectedTab] = useState(0);

  useEffect(() => {
    setSelectedTab(viewPagerPos);
  }, [viewPagerPos]);

  return (
    <View style={styles.container}>
      <TouchableRipple
        style={styles.tabContainer}
        onPress={() => {
          setSelectedTab(0);
          onTabPress(0);
        }}>
        <View style={styles.tab}>
          <View style={styles.iconWrapper}>
            <Image
              source={ImageUtils.loadImageWithKey(KEY_ICON_NEW_ORDERS)}
              style={{
                ...styles.icon,
                width: selectedTab == 0 ? 28 : 24,
                height: selectedTab == 0 ? 28 : 24,
              }}
            />
          </View>
          <Text
            style={{
              ...styles.tabLabel,
              fontWeight: selectedTab == 0 ? 'bold' : 'normal',
            }}>
            {locale.getTranslatedString(
              'tab_new',
              'NEW',
              appLocaleState.locale,
              appTranslations,
            )}
          </Text>
        </View>
      </TouchableRipple>

      <TouchableRipple
        style={styles.tabContainer}
        onPress={() => {
          setSelectedTab(1);
          onTabPress(1);
        }}>
        <View style={styles.tab}>
          <View style={styles.iconWrapper}>
            <Image
              source={ImageUtils.loadImageWithKey(KEY_ICON_PENDING_ORDERS)}
              style={{
                ...styles.icon,
                width: selectedTab == 1 ? 28 : 24,
                height: selectedTab == 1 ? 28 : 24,
              }}
            />
          </View>
          <Text
            style={{
              ...styles.tabLabel,
              fontWeight: selectedTab == 1 ? 'bold' : 'normal',
            }}>
            {locale.getTranslatedString(
              'tab_pending',
              'PENDING',
              appLocaleState.locale,
              appTranslations,
            )}
          </Text>
        </View>
      </TouchableRipple>

      <TouchableRipple
        style={styles.tabContainer}
        onPress={() => {
          setSelectedTab(2);
          onTabPress(2);
        }}>
        <View style={styles.tab}>
          <View style={styles.iconWrapper}>
            <Image
              source={ImageUtils.loadImageWithKey(KEY_ICON_DONE_ORDERS)}
              style={{
                ...styles.icon,
                width: selectedTab == 2 ? 28 : 24,
                height: selectedTab == 2 ? 28 : 24,
              }}
            />
          </View>
          <Text
            style={{
              ...styles.tabLabel,
              fontWeight: selectedTab == 2 ? 'bold' : 'normal',
            }}>
            {locale.getTranslatedString(
              'tab_ready',
              'READY',
              appLocaleState.locale,
              appTranslations,
            )}
          </Text>
        </View>
      </TouchableRipple>
    </View>
  );
};

export default BottomNavigation;
