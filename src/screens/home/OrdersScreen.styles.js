import {StyleSheet} from 'react-native';

export default theme =>
  StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'space-between',
      alignItems: 'stretch',
      alignContent: 'stretch',
      backgroundColor: theme.colors.background,
    },
    list: {
      flex: 1,
    },
    listContent: {
      flexGrow: 1,
    },
    listSeparator: {
      height: 1,
      backgroundColor: '#B7C0BF',
    },
    loader: {
      flex: 1,
    },
  });
