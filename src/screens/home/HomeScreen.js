/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useState, useEffect, useRef} from 'react';
import {
  SafeAreaView,
  Image,
  View,
  Dimensions,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import {Appbar, Text, Menu, Divider} from 'react-native-paper';
import useStyles from './HomeScreen.styles';
import {GlobalContext} from '../../GlobalContext';
import {
  withNavigation,
  StackActions,
  NavigationActions,
} from 'react-navigation';
import Icon from 'react-native-vector-icons/Entypo';
import ViewPager from '@react-native-community/viewpager';
import BottomNavigation from './components/BottomNavigation';
import ImageUtils from '../../utils/ImageUtils';
import {
  KEY_ICON_DRAWER,
  KEY_ICON_CHECK,
  TYPE_SCREEN_NEW_ORDERS,
  TYPE_SCREEN_PENDING_ORDERS,
  TYPE_SCREEN_READY_ORDERS,
  NOTIFICATION_TYPE_REFRESH_NEW_ORDERS,
  NOTIFICATION_TYPE_SYNC_ORDERS,
} from '../../config/Constants';
import OrdersScreen from './OrdersScreen';
import NewOrdersButton from './components/NewOrdersButton';
import AlertsButton from './components/AlersButton';
import firebase from 'react-native-firebase';

const HomeScreen = ({navigation}) => {
  const {
    ui,
    userRepository,
    appLanguages,
    appLocaleState,
    appTranslations,
    languageRepository,
    locale,
    loginState,
    alerts,
    counters,
  } = useContext(GlobalContext);
  const [isOverflowMenuVisible, setOverflowMenuVisible] = useState(false);
  const [selectedPagePosition, setSelectedPagePosition] = useState(0);
  const [pagesForRefresh, setPagesForRefresh] = useState({
    new: false,
    pending: false,
    ready: false,
  });
  const [expandedOrderId, setExpandedOrderId] = useState();
  const [title, setTitle] = useState();

  const styles = useStyles(ui.theme);

  const viewPagerRef = useRef(null);

  useEffect(() => {
    if (!loginState.isLoggedIn) {
      logoutAction();
    }

    firebase.notifications().onNotification(remoteMessage => {
      if (remoteMessage.data.type == NOTIFICATION_TYPE_REFRESH_NEW_ORDERS) {
        onChangeScreenEvent(0, [0], null);
      } else if (remoteMessage.data.type == NOTIFICATION_TYPE_SYNC_ORDERS) {
        const {order_status} = remoteMessage.data;

        if (order_status == 'processing') {
          setPagesForRefresh({
            new: true,
            pending: true,
            ready: false,
          });
        } else if (
          order_status == 'ready' ||
          order_status == 'delivered' ||
          order_status == 'paid'
        ) {
          setPagesForRefresh({
            new: false,
            pending: true,
            ready: true,
          });
        }
      }
    });

    firebase.messaging().onMessage(message => {
      if (message.data.type == NOTIFICATION_TYPE_SYNC_ORDERS) {
        const {order_status} = message.data;

        if (order_status == 'processing') {
          setPagesForRefresh({
            new: true,
            pending: true,
            ready: false,
          });
        } else if (
          order_status == 'ready' ||
          order_status == 'delivered' ||
          order_status == 'paid'
        ) {
          setPagesForRefresh({
            new: false,
            pending: true,
            ready: true,
          });
        }
      }
    });

    const getUserFromLocalStorageReq = async () => {
      if (!loginState.isLoggedIn) {
        return;
      }

      const response = await userRepository.getUser();
      if (response && !response.error) {
        setTitle(response.data.name);
      }
    };
    getUserFromLocalStorageReq();
  }, [alerts, loginState.isLoggedIn, userRepository]);

  useEffect(() => {
    setPagesForRefresh({
      new: true,
      pending: true,
      ready: true,
    });
  }, [locale.langCode]);

  logoutAction = () => {
    const resetAuthAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Auth'})],
    });
    navigation.dispatch(resetAuthAction);
  };

  resetExpandedOrderId = () => {
    setExpandedOrderId(null);
  };

  onChangeScreenEvent = (position, toBeUpdated, expandedOrderId) => {
    const pages = {
      new: false,
      pending: false,
      ready: false,
    };
    toBeUpdated.forEach(page => {
      switch (page) {
        case 0: {
          pages.new = true;
          break;
        }
        case 1: {
          pages.pending = true;
          break;
        }
        case 2: {
          pages.ready = true;
          break;
        }
      }
    });
    if (expandedOrderId) {
      setExpandedOrderId(expandedOrderId);
    }
    setPagesForRefresh(pages);
    viewPagerRef.current.setPage(position);
  };

  const handleOpenAlertsScreen = () => {
    navigation.navigate('Alerts');
  };

  const handleOpenNewOrdersPage = () => {
    viewPagerRef.current.setPage(0);
  };

  const changeLocale = async (locale) => {
    if (locale == appLocaleState.locale) return;
    const result = await languageRepository.setUserLocale(locale);
    if (result.success) {
      appLocaleState.setAppLocale(locale);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <Appbar style={styles.appBar}>
        <Appbar.Header style={styles.appBarHeader}>
          <Image
            source={ImageUtils.loadImageWithKey(KEY_ICON_DRAWER)}
            style={styles.icon}
          />
          <Text style={styles.title}>{title}</Text>
          <NewOrdersButton
            style={{marginEnd: 8}}
            ordersCount={counters.newOrders}
            onPress={() => handleOpenNewOrdersPage()}
          />
          <AlertsButton
            alertsCount={counters.alerts}
            onPress={() => handleOpenAlertsScreen()}
          />
          <Appbar.Action
            icon={() => <Icon name="dots-three-vertical" size={20} />}
            onPress={() => setOverflowMenuVisible(!isOverflowMenuVisible)}
            collapsable={false}
          />
        </Appbar.Header>
      </Appbar>
      <View>
        <Menu
          visible={isOverflowMenuVisible}
          anchor={{x: Dimensions.get('screen').width - 16, y: 48}}
          onDismiss={() => setOverflowMenuVisible(false)}>
          {appLanguages.map(language => (
            <TouchableOpacity
              onPress={() => {
                setOverflowMenuVisible(false);
                changeLocale(language.locale);
              }}
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <Menu.Item title={language.native} />
              {language.locale == appLocaleState.locale && (
                <Image
                  source={ImageUtils.loadImageWithKey(KEY_ICON_CHECK)}
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: 'stretch',
                    marginRight: 15,
                  }}
                />
              )}
            </TouchableOpacity>
          ))}
          <Divider />
          <Menu.Item
            title={locale.getTranslatedString(
              'logout',
              'Logout',
              appLocaleState.locale,
              appTranslations,
            )}
            onPress={async () => {
              const result = await userRepository.logout();
              if (result.success) {
                setOverflowMenuVisible(false);
                loginState.setLoggedIn(false);
              } else {
                ToastAndroid.show(result.data.message, ToastAndroid.LONG);
              }
            }}
          />
        </Menu>
      </View>
      <ViewPager
        style={{flex: 1}}
        initialPage={0}
        ref={viewPagerRef}
        orientation="horizontal"
        onPageSelected={e => {
          setSelectedPagePosition(e.nativeEvent.position);
        }}>
        <View key="1">
          <OrdersScreen
            type={TYPE_SCREEN_NEW_ORDERS}
            expandedOrderId={expandedOrderId}
            shouldRefresh={pagesForRefresh.new}
            resetExpandedOrderId={resetExpandedOrderId}
            onChangeScreenEvent={onChangeScreenEvent}
            handleResetRefreshState={() =>
              setPagesForRefresh(
                Object.assign({}, pagesForRefresh, {new: false}),
              )
            }
            onOrdersCountObtained={(type, count) => {
              counters.setNewOrders(count);
            }}
          />
        </View>

        <View key="2">
          <OrdersScreen
            type={TYPE_SCREEN_PENDING_ORDERS}
            expandedOrderId={expandedOrderId}
            resetExpandedOrderId={resetExpandedOrderId}
            shouldRefresh={pagesForRefresh.pending}
            onChangeScreenEvent={onChangeScreenEvent}
            handleResetRefreshState={() =>
              setPagesForRefresh(
                Object.assign({}, pagesForRefresh, {pending: false}),
              )
            }
          />
        </View>

        <View key="3">
          <OrdersScreen
            type={TYPE_SCREEN_READY_ORDERS}
            expandedOrderId={expandedOrderId}
            resetExpandedOrderId={resetExpandedOrderId}
            shouldRefresh={pagesForRefresh.ready}
            onChangeScreenEvent={onChangeScreenEvent}
            handleResetRefreshState={() =>
              setPagesForRefresh(
                Object.assign({}, pagesForRefresh, {ready: false}),
              )
            }
          />
        </View>
      </ViewPager>

      <BottomNavigation
        theme={ui.theme}
        locale={locale}
        appLocaleState={appLocaleState}
        appTranslations={appTranslations}
        onTabPress={position => {
          viewPagerRef.current.setPage(position);
        }}
        viewPagerPos={selectedPagePosition}
      />
    </SafeAreaView>
  );
};

export default withNavigation(HomeScreen);
