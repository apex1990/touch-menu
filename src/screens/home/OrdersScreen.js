import React, {useContext, useEffect, useState, memo, useCallback} from 'react';
import {View, RefreshControl, ToastAndroid} from 'react-native';
import useStyles from './OrdersScreen.styles';
import {GlobalContext} from '../../GlobalContext';
import {
  TYPE_SCREEN_NEW_ORDERS,
  TYPE_SCREEN_PENDING_ORDERS,
  TYPE_SCREEN_READY_ORDERS,
} from '../../config/Constants';
import {FlatList} from 'react-native-gesture-handler';
import OrderItem from './components/OrderItem';

const OrdersScreen = ({
  type,
  expandedOrderId,
  resetExpandedOrderId,
  shouldRefresh,
  onChangeScreenEvent,
  handleResetRefreshState,
  onOrdersCountObtained,
}) => {
  const {
    ui,
    ordersRepository,
    appLocaleState,
    appTranslations,
    locale,
  } = useContext(GlobalContext);
  const [orders, setOrders] = useState([]);
  const [visibleFlag, setVisibleFlag] = useState(true);
  const [removedOrderIds, setRemovedOrderIds] = useState([]);
  const [isRefreshing, setRefreshing] = useState(false);
  const styles = useStyles(ui.theme);

  useEffect(() => {
    setRefreshing(true);
  }, []);

  useEffect(() => {
    if (shouldRefresh) {
      setRefreshing(true);
    }
  }, [shouldRefresh]);

  useEffect(() => {
    if (isRefreshing) {
      getOrdersCallback();
    } else {
      if (shouldRefresh) {
        handleResetRefreshState();
      }
    }
  }, [isRefreshing]);

  const onExpansionStateChange = async (type, order, isExpanded) => {
    if (type == TYPE_SCREEN_NEW_ORDERS) {
      const result = await ordersRepository.changeOrderStatus(order.d, type);
      if (result.success) {
        setRefreshing(true);
        onChangeScreenEvent(1, [0, 1], order.d);
      } else {
        ToastAndroid.show(result.data.message, ToastAndroid.LONG);
      }
    } else {
      var updatedOrders = [];
      updatedOrders = orders.map(o => {
        const updatedOrder = {...o};
        if (updatedOrder.id == order.id) {
          updatedOrder.isExpanded = isExpanded;
        } else {
          updatedOrder.isExpanded = false;
        }

        if (updatedOrder.d == expandedOrderId) {
          resetExpandedOrderId();
        }
        return updatedOrder;
      });
      setOrders(updatedOrders);
    }
  };

  const onCompleteOrderAction = async (type, order) => {
    const result = await ordersRepository.changeOrderStatus(order.d, type);
    if (result.success) {
      setRemovedOrderIds(ids => {
        ids.push(order.id);
        return ids;
      });

      setOrders(orders => {
        return orders.filter(i => i.id !== order.id);
      });

      onChangeScreenEvent(1, [2]);
    } else {
      ToastAndroid.show(result.data.message, ToastAndroid.LONG);
    }
  };

  const onPendingOrderAction = async (type, order) => {
    const result = await ordersRepository.pendingOrderStatus(order.d, type);
    if (result.success) {
      // setRemovedOrderIds(ids => {
      //   ids.push(order.id);
      //   return ids;
      // });

      // setOrders(orders => {
      //   return orders.filter(i => i.id !== order.id);
      // });
      setVisibleFlag(false);
      // setRefreshing(true);
      // onChangeScreenEvent(1, [2]);
    } else {
      ToastAndroid.show(result.data.message, ToastAndroid.LONG);
      setVisibleFlag(true)
    }
  }

  const getOrders = async () => {
    switch (type) {
      case TYPE_SCREEN_NEW_ORDERS: {
        const result = await ordersRepository.getNewOrdersList();
        if (result.success) {
          setOrders(() => {
            const orders = result.data.orders.map(o => {
              const updatedOrder = {...o};
              if (updatedOrder.d == expandedOrderId) {
                updatedOrder.isExpanded = true;
              }
              return updatedOrder;
            });

            if (onOrdersCountObtained)
              onOrdersCountObtained(type, orders.length);
            return orders;
          });
        } else {
          ToastAndroid.show(result.data.message, ToastAndroid.LONG);
        }
        break;
      }
      case TYPE_SCREEN_PENDING_ORDERS: {
        const result = await ordersRepository.getPendingOrdersList();
        if (result.success) {
          setOrders(() => {
            return result.data.orders.map(o => {
              const updatedOrder = {...o};
              if (updatedOrder.d == expandedOrderId) {
                updatedOrder.isExpanded = true;
              }
              return updatedOrder;
            });
          });
        } else {
          ToastAndroid.show(result.data.message, ToastAndroid.LONG);
        }
        break;
      }
      case TYPE_SCREEN_READY_ORDERS: {
        const result = await ordersRepository.getReadyOrdersList();
        if (result.success) {
          setOrders(() => {
            return result.data.orders.map(o => {
              const updatedOrder = {...o};
              if (updatedOrder.d == expandedOrderId) {
                updatedOrder.isExpanded = true;
              }
              return updatedOrder;
            });
          });
          setRefreshing(false);
        } else {
          ToastAndroid.show(result.data.message, ToastAndroid.LONG);
          setRefreshing(false);
        }
        break;
      }
      default:
        throw new Error('Incorrect type...');
    }

    setRefreshing(false);
  };

  const getOrdersCallback = useCallback(() => getOrders(), [isRefreshing]);

  const renderOrderItem = item => {
    return (
      <OrderItem
        theme={ui.theme}
        type={type}
        data={item}
        locale={locale}
        appTranslations={appTranslations}
        appLocaleState={appLocaleState}
        onExpansionStateChange={onExpansionStateChange}
        onCompleteOrderAction={onCompleteOrderAction}
        onPendingOrderAction={onPendingOrderAction}
        refreshOrdersList={() => setRefreshing(true)}
        visibleFlag={visibleFlag}
      />
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={orders.filter(i => removedOrderIds.indexOf(i.id) === -1)}
        extraData={orders}
        style={styles.list}
        contentContainerStyle={styles.listContent}
        initialNumToRender={2}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => setRefreshing(true)}
          />
        }
        renderItem={({item}) =>
          renderOrderItem(item, onExpansionStateChange, onCompleteOrderAction)
        }
        refreshing={true}
        keyExtractor={item => item.id}
        ItemSeparatorComponent={() => <View style={styles.listSeparator} />}
      />
    </View>
  );
};

export default OrdersScreen;
