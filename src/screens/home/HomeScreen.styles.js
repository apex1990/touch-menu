import {StyleSheet} from 'react-native';

export default theme => {
  return StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: 'stretch',
      alignContent: 'stretch',
      backgroundColor: theme.colors.background,
    },
    appBar: {
      backgroundColor: 'transparent',
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: 'stretch',
      alignContent: 'stretch',
      elevation: 0,
    },
    appBarHeader: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: 'transparent',
    },
    icon: {
      width: 48,
      height: 48,
    },
    title: {
      flex: 1,
      fontSize: 20,
      paddingStart: 8,
      paddingEnd: 8,
    },
    loader: {
      width: 60,
      height: 60,
      position: 'absolute',
      left: '50%',
      bottom: '50%',
      marginLeft: -30,
      marginTop: -30,
    },
  });
};
