import React, {useEffect, useState} from 'react';
import {Dialog, Text, TouchableRipple} from 'react-native-paper';
import {View, FlatList} from 'react-native';
import useStyle from './PickLanguageDialog.styles';

const PickLanguageDialog = ({theme, locale, isVisible, onDismiss}) => {
  const [languagesList, setLanguagesList] = useState([]);

  const styles = useStyle(theme);

  useEffect(() => {
    const languages = locale.getSupportedLanguages();
    if (languages != null) {
      setLanguagesList(languages);
    }
  }, [isVisible]);

  const handleLanguageSelect = item => {
    locale.changeLanguage(item.code);
    onDismiss();
  };

  const renderLangugeItem = (item, _) => {
    return (
      <TouchableRipple onPress={() => handleLanguageSelect(item)}>
        <View style={styles.listItem}>
          <Text style={styles.itemTitle}>{item.name}</Text>
        </View>
      </TouchableRipple>
    );
  };

  return (
    <Dialog visible={isVisible} onDismiss={() => onDismiss()}>
      <Dialog.Title>
        {locale.getLocalizedString('dialog_language', 'Select a language:')}
      </Dialog.Title>
      <Dialog.Content>
        <FlatList
          data={languagesList}
          extraData={languagesList}
          renderItem={({item, index}) => renderLangugeItem(item, index)}
          keyExtractor={(_, index) => String(index)}
          style={styles.list}
        />
      </Dialog.Content>
    </Dialog>
  );
};

export default PickLanguageDialog;
