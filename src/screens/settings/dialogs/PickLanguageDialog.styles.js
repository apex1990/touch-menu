import {StyleSheet} from 'react-native';

export default theme =>
  StyleSheet.create({
    list: {},
    listItem: {
      marginTop: 4,
      marginBottom: 4,
    },
    itemTitle: {
      textAlignVertical: 'center',
      margin: 8,
      fontWeight: '600',
    },
  });
