import React, {useContext, useState, useEffect} from 'react';
import {SafeAreaView, View, FlatList} from 'react-native';
import {withNavigation} from 'react-navigation';
import {
  Appbar,
  Text,
  Divider,
  TouchableRipple,
  Portal,
} from 'react-native-paper';
import useStyles from './SettingsScreen.styles';
import {GlobalContext} from '../../GlobalContext';
import {SETTINGS_LANGUAGE} from '../../config/Constants';
import PickLanguageDialog from './dialogs/PickLanguageDialog';

const SettingsScreen = ({navigation}) => {
  const {ui, locale} = useContext(GlobalContext);
  const styles = useStyles(ui.theme);

  const [isLangPickDialogVisible, setLangPickDialogVisible] = useState(false);

  const [settingsList, setSettingsList] = useState([]);
  const [shouldRefresh, setShouldRefresh] = useState(false);

  useEffect(() => {
    const settings = [
      {
        type: SETTINGS_LANGUAGE,
        title: locale.getLocalizedString('language', 'Language'),
      },
    ];
    setSettingsList(settings);
  }, []);

  useEffect(() => {
    const settings = [
      {
        type: SETTINGS_LANGUAGE,
        title: locale.getLocalizedString('language', 'Language'),
      },
    ];
    setSettingsList(settings);
    setShouldRefresh(false);
  }, [shouldRefresh]);

  const handleItemTapAction = item => {
    if (item.type == SETTINGS_LANGUAGE) {
      setLangPickDialogVisible(true);
    }
  };

  const renderSettingsItem = (item, index) => {
    return (
      <TouchableRipple onPress={() => handleItemTapAction(item)}>
        <View style={styles.listItem}>
          <Text style={styles.itemTitle}>{item.title}</Text>
          <Divider />
        </View>
      </TouchableRipple>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <Appbar style={styles.appBar}>
        <Appbar.Header style={styles.appBarHeader}>
          <Appbar.BackAction onPress={() => navigation.goBack()} />
          <Text style={styles.appBarTitle}>
            {locale.getLocalizedString('settings', 'Settings')}
          </Text>
        </Appbar.Header>
      </Appbar>

      <View style={{flex: 1}}>
        <FlatList
          data={settingsList}
          extraData={settingsList}
          renderItem={({item, index}) => renderSettingsItem(item, index)}
          keyExtractor={({_, index}) => String(index)}
          style={styles.list}
        />
      </View>

      <Portal>
        <PickLanguageDialog
          theme={ui.theme}
          locale={locale}
          isVisible={isLangPickDialogVisible}
          onDismiss={() => {
            setLangPickDialogVisible(false);
            setShouldRefresh(true);
          }}
        />
      </Portal>
    </SafeAreaView>
  );
};

export default withNavigation(SettingsScreen);
